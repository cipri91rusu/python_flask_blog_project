# Python_Flask_Blog_Project

The current projects represents a full implementation of a server side rendered "blog-like" application, which renders/runs a CRUD application, allowing the final user to apply CRUD operations to a series of "blogposts", rendering blocks of text & images (if uploaded), allowing certain operations depending on the permissions.

The application requires a connection to a database (locally used PostrgreSQL w/ SQLAlchemy) to permanently store all data, creating a default **admin** user, as superuser.

Besides the basic application which runs and renders the pages server side, there's also an implementation of several API routes, that return all the stored information in JSON format and several user management pages (available only to the superuser), where the superuser has CRUD acces to the users.
